This repo implements the document binarization approach described in "Document Binarization via Multi-Resolutional Attention Model with DRD Loss"


To evaluate the pretrained model, please download it from:
https://pan.baidu.com/s/1eLfh_9yKnVxBHSfsdHy8vg


Then run the script: python -u seg_convcrf.py --input-csv $test_db --model-file $input_model_path --out-dir $output_path
