import warnings

with warnings.catch_warnings():
    from keras.layers.convolutional import Conv2D, MaxPooling2D
    from keras.layers import (
        Input,
        multiply,
        merge,
        Activation,
        UpSampling2D,
        AveragePooling2D,
        concatenate,
    )
    from keras.layers.merge import add
    from keras.layers.normalization import BatchNormalization
    from keras.models import Model
    from keras.regularizers import l2
    from keras import backend as K


class MultiResolutionAttentionModel:
    def _conv_block(self, **conv_params):
        bn_axis = 3 if K.image_data_format() == "channels_last" else 1

        filters = conv_params["filters"]
        strides = conv_params.setdefault("strides", (1, 1))
        padding = conv_params.setdefault("padding", "same")
        kernel_size = conv_params["kernel_size"]
        kernel_initializer = conv_params.setdefault("kernel_initializer", "he_normal")
        kernel_regularizer = conv_params.setdefault("kernel_regularizer", l2(1.0e-4))

        def f(input):
            x = Conv2D(
                filters=filters,
                kernel_size=kernel_size,
                padding=padding,
                strides=strides,
            )(input)
            x = BatchNormalization(axis=bn_axis)(x)
            x = Activation("relu")(x)

            x = Conv2D(
                filters=filters,
                kernel_size=kernel_size,
                padding=padding,
                strides=strides,
            )(x)
            x = BatchNormalization(axis=bn_axis)(x)

            shortcut = Conv2D(
                filters=filters,
                kernel_size=kernel_size,
                padding=padding,
                strides=strides,
            )(input)
            shortcut = BatchNormalization(axis=bn_axis)(shortcut)

            x = add([x, shortcut])
            return Activation("relu")(x)

        return f

    def _bn_relu(self, input):
        """Helper to build a BN -> relu block
        """
        norm = BatchNormalization(axis=3)(input)
        return Activation("relu")(norm)

    def _conv_bn_relu(self, **conv_params):
        """Helper to build a conv -> BN -> relu block
        """
        filters = conv_params["filters"]
        strides = conv_params.setdefault("strides", (1, 1))
        padding = conv_params.setdefault("padding", "same")
        kernel_size = conv_params["kernel_size"]
        kernel_initializer = conv_params.setdefault("kernel_initializer", "he_normal")
        kernel_regularizer = conv_params.setdefault("kernel_regularizer", l2(1.0e-4))

        def f(input):
            conv = Conv2D(
                filters=filters,
                kernel_size=kernel_size,
                strides=strides,
                padding=padding,
                kernel_initializer=kernel_initializer,
                kernel_regularizer=kernel_regularizer,
            )(input)
            return self._bn_relu(conv)

        return f

    def _conv_attention(self, **conv_params):
        filters = conv_params["filters"]
        strides = conv_params.setdefault("strides", (1, 1))
        padding = conv_params.setdefault("padding", "same")
        kernel_size = conv_params["kernel_size"]
        kernel_initializer = conv_params.setdefault("kernel_initializer", "he_normal")
        kernel_regularizer = conv_params.setdefault("kernel_regularizer", l2(1.0e-4))

        def f(input):
            attention_probs = Conv2D(
                filters=filters,
                kernel_size=kernel_size,
                strides=strides,
                padding=padding,
                kernel_initializer=kernel_initializer,
                kernel_regularizer=kernel_regularizer,
                activation="softmax",
            )(input)
            return multiply([input, attention_probs])
            # return merge([input, attention_probs], mode='mul')

        return f

    def _encoder_decoder(self):
        def f(input):
            bn_axis = 3 if K.image_data_format() == "channels_last" else 1

            # encoder
            conv1 = self._conv_bn_relu(filters=16, kernel_size=(5, 5))(input)
            pool1 = MaxPooling2D((2, 2), padding="same")(conv1)  # 128x128

            conv2 = self._conv_block(filters=32, kernel_size=(3, 3))(pool1)
            conv2 = self._conv_bn_relu(filters=32, kernel_size=(3, 3))(conv2)
            pool2 = MaxPooling2D((2, 2), padding="same")(conv2)  # 64x64

            conv3 = self._conv_block(filters=64, kernel_size=(3, 3))(pool2)
            conv3 = self._conv_bn_relu(filters=64, kernel_size=(3, 3))(conv3)
            pool3 = MaxPooling2D((2, 2), padding="same")(conv3)  # 32x32

            conv4 = self._conv_block(filters=128, kernel_size=(3, 3))(pool3)
            conv4 = self._conv_bn_relu(filters=128, kernel_size=(3, 3))(conv4)
            pool4 = MaxPooling2D((2, 2), padding="same")(conv4)  # 16x16

            # attention block
            attd5 = self._conv_attention(filters=128, kernel_size=(5, 5))(pool4)

            # decoder
            up6 = self._conv_block(filters=128, kernel_size=(3, 3))(
                UpSampling2D((2, 2))(attd5)
            )  # 32x32
            merge6 = concatenate([conv4, up6], axis=-1)
            attd6 = self._conv_attention(filters=256, kernel_size=(3, 3))(merge6)
            conv6 = self._conv_bn_relu(filters=128, kernel_size=(3, 3))(attd6)

            up7 = self._conv_block(filters=64, kernel_size=(3, 3))(
                UpSampling2D((2, 2))(conv6)
            )  # 64x64
            merge7 = concatenate([conv3, up7], axis=-1)
            attd7 = self._conv_attention(filters=128, kernel_size=(3, 3))(merge7)
            conv7 = self._conv_bn_relu(filters=64, kernel_size=(3, 3))(attd7)

            up8 = self._conv_block(filters=32, kernel_size=(3, 3))(
                UpSampling2D((2, 2))(conv7)
            )  # 128x128
            merge8 = concatenate([conv2, up8], axis=-1)
            attd8 = self._conv_attention(filters=64, kernel_size=(3, 3))(merge8)
            conv8 = self._conv_bn_relu(filters=32, kernel_size=(3, 3))(attd8)

            up9 = self._conv_bn_relu(filters=16, kernel_size=(3, 3))(
                UpSampling2D((2, 2))(conv8)
            )  # 256x256
            return concatenate([conv1, up9], axis=-1)

        return f

    def build_model(self, **params):
        inputs = Input(shape=(None, None, 3))

        i1 = inputs
        i2 = AveragePooling2D((2, 2))(inputs)
        i3 = AveragePooling2D((4, 4))(inputs)

        b1 = self._encoder_decoder()(i1)
        b2 = self._encoder_decoder()(i2)
        b3 = self._encoder_decoder()(i3)

        # upsample
        f1 = b1
        f2 = UpSampling2D((2, 2))(b2)
        f3 = UpSampling2D((4, 4))(b3)

        # merge and attention again
        atten1 = self._conv_attention(filters=32, kernel_size=(5, 5))(f1)
        atten2 = self._conv_attention(filters=32, kernel_size=(5, 5))(f2)
        atten3 = self._conv_attention(filters=32, kernel_size=(5, 5))(f3)

        output = concatenate([atten1, atten2, atten3], axis=-1)
        output = Conv2D(2, (3, 3), activation="softmax", padding="same")(output)

        return Model(inputs, output)
