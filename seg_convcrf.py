import warnings

with warnings.catch_warnings():
    import os, sys, csv
    import re
    import cv2
    import argparse
    import keras
    from keras import backend as K
    import tensorflow as tf
    import numpy as np
    from net import MultiResolutionAttentionModel
    import convcrf


class RunMultiAttentionDocBinarization:
    def __init__(self, model):
        self.mod = 64
        self.model = model

    def run_seg(self, image):
        self.image = image
        border = tuple([(self.mod - x % self.mod) % self.mod for x in self.image.shape])
        img = cv2.copyMakeBorder(
            self.image, 0, border[0], 0, border[1], cv2.BORDER_REPLICATE, value=255
        )

        img = img.astype("float32") / 255.0
        img = np.reshape(img, (1, img.shape[0], img.shape[1], 3))

        prob = self.model.predict(img, batch_size=1, verbose=0)[0]
        prob = prob[0 : prob.shape[0] - border[0], 0 : prob.shape[1] - border[1], :]

        return prob


def remove_clutters(image=None):
    """image: input image (black:text, white:background)
    """
    # height & width statistics
    ccs = cv2.connectedComponentsWithStats(255 - image, 8, cv2.CV_32S)
    mean_h = np.mean(ccs[2][1:, cv2.CC_STAT_HEIGHT])
    stdvar_h = np.std(ccs[2][1:, cv2.CC_STAT_HEIGHT])
    mean_w = np.mean(ccs[2][1:, cv2.CC_STAT_WIDTH])
    stdvar_w = np.std(ccs[2][1:, cv2.CC_STAT_WIDTH])

    # stroke width statistics
    sw = np.zeros(ccs[0], np.float32)
    dt = cv2.distanceTransform(255 - image, cv2.DIST_L2, 5)
    for i in range(1, ccs[0]):
        sw[i] = (np.sum((ccs[1] == i) * dt)) / ccs[2][i, cv2.CC_STAT_AREA]

    mean_sw = np.mean(sw)
    stdvar_sw = np.std(sw)

    # eigen statistics
    evals = np.ones(ccs[0], np.float32)
    for i in range(1, ccs[0]):
        y, x = np.nonzero(ccs[1] == i)
        cx, cy = np.mean(x), np.mean(y)
        coords = np.vstack([x - cx, y - cy])
        cov = np.ma.cov(coords)
        vals, vecs = np.linalg.eig(cov)
        evals[i] = (
            np.inf if np.count_nonzero(vals) != 2 else np.divide(vals[0], vals[1])
        )

    # find outliers
    outlier_h = np.nonzero(ccs[2][:, cv2.CC_STAT_HEIGHT] > mean_h + 3 * stdvar_h)[
        0
    ].tolist()
    outlier_w = np.nonzero(ccs[2][:, cv2.CC_STAT_WIDTH] > mean_w + 3 * stdvar_w)[
        0
    ].tolist()
    outlier_sw = np.nonzero(sw > mean_sw + 3 * stdvar_sw)[0].tolist()

    mask = np.zeros(image.shape, np.int64)
    mask += 255 * (ccs[1] == 0)
    for i in list(
        (set(outlier_h) & set(outlier_sw)) | (set(outlier_w) & set(outlier_sw))
    ):
        mask += 255 * (ccs[1] == i)

    # find outliers again
    outlier_h = np.nonzero(ccs[2][:, cv2.CC_STAT_HEIGHT] > 0.3 * image.shape[0])[
        0
    ].tolist()
    outlier_w = np.nonzero(ccs[2][:, cv2.CC_STAT_WIDTH] > 0.3 * image.shape[1])[
        0
    ].tolist()
    outlier_e = (
        np.nonzero(evals > 50)[0].tolist() + np.nonzero(evals < 0.02)[0].tolist()
    )
    for i in list(
        (set(outlier_h) & set(outlier_e)) | (set(outlier_w) & set(outlier_e))
    ):
        mask += 255 * (ccs[1] == i)

    return mask.astype(np.uint8)


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parser = argparse.ArgumentParser(description="Text line segmentation.")
    parser.add_argument("-i", "--input-csv", dest="input_db", help="The input dataset")
    parser.add_argument(
        "-m", "--model-file", dest="model", help="The pretrained CNN model"
    )
    parser.add_argument(
        "-o", "--out-dir", dest="out_dir", help="The output dir for segmentation files"
    )
    args = parser.parse_args()

    # using CPU
    config = tf.ConfigProto(device_count={"GPU": 0})
    sess = tf.Session(config=config)
    K.set_session(sess)

    # create network and copy weights from model
    if os.path.isfile(args.model):
        model = MultiResolutionAttentionModel().build_model()
        model.load_weights(args.model)
    else:
        raise IOError("Load pre-trained model failed: " + args.model)

    seg = RunMultiAttentionDocBinarization(model)

    # load input images
    with open(args.input_db, "r") as in_fh:
        reader = csv.reader(in_fh)
        for row in reader:
            prefix = re.sub(r".*\/", r"", os.path.dirname(row[0]))
            basename = os.path.splitext(os.path.basename(row[0]))[0]

            image = cv2.imread(row[0], cv2.IMREAD_COLOR)

            # save heatmap
            prob = seg.run_seg(image)

            # prob = prob.transpose(2, 0, 1)
            # prediction = (255*np.argmax(prob, axis=0)).astype(np.uint8)

            prediction = np.squeeze(convcrf.do_crf_inference(image, prob))
            prediction = remove_clutters(prediction)
            _, prediction = cv2.threshold(prediction, 127, 255, cv2.THRESH_BINARY)

            cv2.imwrite(
                os.path.join(args.out_dir, prefix + "-" + basename + ".bmp"), prediction
            )

            print("Done:", prefix + "-" + basename)


if __name__ == "__main__":
    sys.exit(main())
